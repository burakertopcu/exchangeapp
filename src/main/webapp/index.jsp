<html lang="tr">

<head>
	<%@ page pageEncoding="UTF-8" %>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- External CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/brands.css">
	
	<!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Josefin+Sans:300,400,700">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	
	<!-- CSS -->
    <link rel="stylesheet" href="css/style.min.css">
	
	<!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
	
<script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
	
	

</head>

<body data-spy="scroll" data-target="#navbar" class="static-layout">

<section id="gtco-reservation" class="bg-fixed bg-white section-padding overlay" style="background-image: url(img/reservation-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="section-content bg-white p-5 shadow">
                    <div class="heading-section text-center">
                        <span class="subheading">
                            ExchangeApp
                        </span>
                        <h2>Giriş</h2>
                    </div>
                    <div class="input-group md-form form-sm form-2 pl-0">
	<form id="loginForm">
	<div class="row">
		<div class="col-md-12 form-group">
		Kullanıcı adı:
		</div>
		<div class="col-md-12 form-group">
		<input type="text" class="form-control my-0 py-1 red-border" id="username" name="username" />
		</div>
		
		<div class="col-md-12 form-group">
		Şifre:
		</div>
		<div class="col-md-12 form-group">
		<input type="password" class="form-control my-0 py-1 red-border" id="pwd" />
		</div>
		
		<div class="col-md-12 form-group">
		<input type="submit" class="btn btn-primary btn-shadow btn-lg" id="loginbtn" name="loginbtn" value="Giriş Yap/Üye ol" />
		</div>
		
	</table>
	</form>
	
</div>
                </div>
            </div>
        </div>
        
    </div>
</section>





<script>
sessionStorage.clear();

//var request;
            $("#loginForm").submit(function(){
                var username = $("#username").val();
                var password = $("#pwd").val();
                
                if(username == ""){
                    //$('#messageDiv').css("display","none");
                    alert("Username is required");
                    return;
                }
                if(password == ""){
                    //$('#messageDiv').css("display","none");
                    alert("Password is required");
                    return;
                }
                
                $.ajax({
                	url : "login",
                    type : 'POST',
                    dataType: 'json',
                    data : { "username": username, "pwd": password },
                    success : function(data){
                        if(data.isValid){
                        	//alert(username);
                        	sessionStorage.setItem("username", username);
                        	//sessionStorage.setItem("pwd", pwd);
                        	result = data;
                        	sessionStorage.setItem("id", result["id"]);
                        	window.location.href="home.jsp";
                            
                        }else{
                            alert("geçersiz kullanıcı!");
                        }
                    }
                });
                
                return false;
            });
             
            
</script>


</body>
</html>
