<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
	
	<!-- External CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/select2/select2.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/brands.css">
	
	<!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Josefin+Sans:300,400,700">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	
	<!-- CSS -->
    <link rel="stylesheet" href="css/style.min.css">
	
	<!-- Modernizr JS for IE8 support of HTML5 elements and media queries -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

<script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
</head>
<body data-spy="scroll" data-target="#navbar" class="static-layout" style="background-color: #ffff">
	<div id="side-nav" class="sidenav">
	<a href="javascript:void(0)" id="side-nav-close">&times;</a>

	<div class="sidenav-content">
		<p>
			Burak Ertopcu
		</p>
		<p>
			<span class="fs-16 primary-color">05375574958</span>
		</p>
		<p>burakertopcu2634@gmail.com</p>
	</div>
</div>	

<div id="canvas-overlay"></div>
	<div class="boxed-page" style="background-color: #ffff">
		<nav id="navbar-header" class="navbar fixed-top navbar-expand-lg" style="background-color:#ffff">
    <div class="container">
        <a class="navbar-brand navbar-brand-center d-flex align-items-center p-0 only-mobile" href="#mainpage">
            <img src="img/logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="lnr lnr-menu"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
            <ul class="navbar-nav d-flex justify-content-between">
                <li class="nav-item only-desktop">
                    <a class="nav-link" id="side-nav-open" href="#">
                        <span class="lnr lnr-menu"></span>
                    </a>
                </li>
                <div class="d-flex flex-lg-row flex-column">
                    <li class="nav-item active">
                        <a class="nav-link" href="#mainpage">Ana Sayfa <span class="sr-only">(current)</span></a>
                    </li>
                    
                    
                </div>
            </ul>

            <a class="navbar-brand navbar-brand-center d-flex align-items-center only-desktop" href="#mainpage">
                <img src="img/logo.png" alt="">
            </a>
            <ul class="navbar-nav d-flex justify-content-between">
                <div class="d-flex flex-lg-row flex-column">
                    

                    <li class="nav-item dropdown">
                        <a class="nav-link" href="index.jsp">Çıkış Yap</a>
                    </li>
                </div>
                
            </ul>
        </div>
    </div>
</nav>
</div>
<br>
<section id="mainpage" class="bg-white section-padding">
    <div class="container">
        <div class="section-content">
            <div class="row">

                <div class="col-sm-7 py-5 pl-md-0 pl-4">
                    <div class="heading-section pl-lg-5 ml-md-5">
                        <span class="subheading">
                            ExchangeApp
                        </span>
                        <h2>
                            <div id="welcomeMsg"></div>	
                        </h2>

                    </div>
                    
                    <div class="pl-lg-5 ml-md-5">


<table class ="table">
	<tr>
		<th>Hesap Bakiyeleri:</th>
		<th></th>
		<th></th>
	</tr>
	
	<tr>
		<td><b>TL:</b></td>
		<td><b>EURO:</b></td>
		<td><b>DOLAR:</b></td>
	</tr>

	<tr>
		<td><div id="baltl">0</div></td>
		<td><div id="baleu">0</div></td>
		<td><div id="baldolar">0</div></td>
	</tr>
</table>



<table class="table">
	<tr>
		<th>Güncel Kurlar:</th>
		<th>Euro(Alış):</th>
		<th>Dolar(Alış):</th>
	</tr>
	
	<tr>
		<td>TL:</td>
		<td><div id="tlbaseeuro"></div></td>
		<td><div id="tlbasedolar"></div></td>
	</tr>
</table>


<br>
<form id="buyForm" name="buyForm">
<table class="table">
	<tr>
		<th>Döviz Al</th>
		<th></th>
	</tr>
	<tr>
		<td><input type="text" class="form-control my-0 py-1 red-border" id="amount" name="amount" value="0"/></td>
		<td>
			<select class="form-control" id="buyCurr">
  				<option value="euro">Euro</option>
  				<option value="dolar">Dolar</option>	
			</select>
		</td>
	</tr>
	<tr>
		<td><input type="submit" class="btn btn-primary btn-shadow btn-lg" value="Alım Yap"/></td>
		<td></td>
	</tr>
</table>
</form>

<form id="sellForm" name="sellForm">
<table class="table">
	<tr>
		<th>Döviz Sat</th>
		<th></th>
	</tr>
	<tr>
		<td><input type="text" class="form-control my-0 py-1 red-border" id="sellAmount" name="sellAmount" value="0" /></td>
		<td>
			<select class="form-control" id="sellCurr">
  				<option value="euro">Euro</option>
  				<option value="dolar">Dolar</option>	
			</select>
		</td>
	</tr>
	<tr>
		<td><input type="submit" class="btn btn-primary btn-shadow btn-lg" id="btnSell" name="btnSell" value="Satış Yap"/></td>
		<td></td>
	</tr>
</table>
</form>

<div id="transAct">
</div>
                    
                    <div class="pl-lg-5 ml-md-5">
                        
                        <h3 class="mt-5">NOT: Döviz kurları alım-satım işlemleri sırasında anlık olarak güncellenmektedir.</h3>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	



<script>
var username = sessionStorage.getItem('username');
var id = sessionStorage.getItem('id');
if(id == null){
	window.location.href="index.jsp";
}
message = "Hoşgeldiniz Sn. "+username+"!";
//alert(message);
document.getElementById("welcomeMsg").innerHTML = message;


getInfo();

function getInfo(){
	getBalance();
	getCurrencyRates();
	getTransactions();
}


function getBalance(){
	$.ajax({
		url : "balance",
	    type : 'POST',
	    dataType: 'json',
	    data : { "id": id },
	    success : function(data){
	        	//alert(username);
	        	//sessionStorage.setItem("username", username);
	        	//sessionStorage.setItem("pwd", pwd);
	        	result = data["balances"];
	        	document.getElementById("baltl").innerHTML = result[0];
	        	document.getElementById("baleu").innerHTML = result[1];
	        	document.getElementById("baldolar").innerHTML = result[2];
	        	
	        	//sessionStorage.setItem("id", result["id"]);
	        	//window.location.href="home.jsp";
	            
	        
	    }
	});
}

function getCurrencyRates(){
	$.ajax({
		url : "currency",
	    type : 'POST',
	    dataType: 'json',
	    data : { "base": "USD" },
	    success : function(data){
	        	//alert(username);
	        	//sessionStorage.setItem("username", username);
	        	//sessionStorage.setItem("pwd", pwd);
	        	result = data["currencyResult"];
	        	//alert(result);
	        	document.getElementById("tlbasedolar").innerHTML = result;
	        	
	        	//sessionStorage.setItem("id", result["id"]);
	        	//window.location.href="home.jsp";
	            
	        
	    }
	});

	$.ajax({
		url : "currency",
	    type : 'POST',
	    dataType: 'json',
	    data : { "base": "EUR" },
	    success : function(data){
	        	//alert(username);
	        	//sessionStorage.setItem("username", username);
	        	//sessionStorage.setItem("pwd", pwd);
	        	result = data["currencyResult"];
	        	//alert(result);
	        	document.getElementById("tlbaseeuro").innerHTML = result;
	        	
	        	//sessionStorage.setItem("id", result["id"]);
	        	//window.location.href="home.jsp";
	            
	        
	    }
	});
}

function getTransactions(){
	
	$.ajax({
		url : "transacts",
	    type : 'POST',
	    dataType: 'json',
	    data : { "id": id },
	    success : function(data){
	        	//alert(username);
	        	//sessionStorage.setItem("username", username);
	        	//sessionStorage.setItem("pwd", pwd);
	        	result = data["result"];
	        	document.getElementById("transAct").innerHTML = result;
	        	
	        	//sessionStorage.setItem("id", result["id"]);
	        	//window.location.href="home.jsp";
	            
	        
	    }
	});
}


$("#buyForm").submit(function(){
	var id = sessionStorage.getItem('id');
	var amount = document.getElementById("amount").value;
	var curOpt = document.getElementById("buyCurr");
    var buyCurr = curOpt.value;
	var opCode = "1";
	//alert("istenen alış işlemi: "+amount+" "+buyCurr+" id:"+id+" opcode:"+opCode);
    $.ajax({
    	url : "buysell",
        type : 'POST',
        dataType: 'json',
        data : { "id": id, "amount": amount, "currtype": buyCurr, "opcode": opCode },
        success : function(data){
            	//alert("içerde Buy...");
            	result = data["result"];
            	if(result[0] ==0 && result[1] == 0){
            		alert("bakiye yetersiz!");
            		document.getElementById("amount").value="";
            	}
            	else{
            		alert(result[0]+" kuru ile maliyet:"+result[1]+" TL");
            		getInfo();
            		document.getElementById("amount").value="";
            	}
        }
    });
    
    return false;
});

$("#sellForm").submit(function(){
	var id = sessionStorage.getItem('id');
	var amount = document.getElementById("sellAmount").value;
	var curOpt = document.getElementById("sellCurr");
    var sellCurr = curOpt.value;
	var opCode = "2";
    //alert("istenen satış işlemi: "+amount+" "+sellCurr+" id:"+id+" opcode:"+opCode);
    $.ajax({
    	url : "buysell",
        type : 'POST',
        dataType: 'json',
        data : { "opcode": opCode, "id": id, "amount": amount, "currtype": sellCurr },
        success : function(data){
        	//alert("içerde Sell...");
        	result = data["result"];
        	if(result[0] == 0 && result[1] == 0){
        		alert("bakiye yetersiz!");
        		document.getElementById("sellAmount").value = "";	
        	}
        	else{
        		alert(result[0]+" kuru ile "+result[1]+" TL elde ettiniz.");
        		getInfo();
        		document.getElementById("sellAmount").value = "";
        	}
        	
        }
    });
    
    return false;
});

</script>

<!-- External JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="vendor/bootstrap/popper.min.js"></script>
	<script src="vendor/bootstrap/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js "></script>
	<script src="vendor/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js"></script>
	<script src="vendor/stellar/jquery.stellar.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

	<!-- Main JS -->
	<script src="js/app.min.js "></script>

</body>
</html>