package controllers;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.google.gson.JsonObject;

import controllers.BalanceController;
import controllers.CurrencyController;

public class ExchangeController {
	CurrencyController currCont = new CurrencyController();
	BalanceController balCont = new BalanceController();
	
	public double[] buyCurr(int uid, String buyCurr, double amount ){
		double curRate = 0;
		double bought = 0;
		
		double[] result = {curRate, bought};
		return result;
	}
	
	public double[] sellCurr(String uid, String sellCurr, String amount) throws IOException{
		double curRate = 0;
		double sellRevenue = 0; 
		double[] balance={0,0,0};
		String base="";
		double miktar = Double.valueOf(amount);
		
		
		if(sellCurr.equals("euro"))
			base = "EUR";
		else if(sellCurr.equals("dolar"))
			base = "USD";
		
		JsonObject currencyResult= currCont.getCurrencyRates(base);
		String scurRate = currencyResult.get("TRY").toString();
		
		balance = balCont.getBalance(Integer.valueOf(uid));
		
		if(base.equals("EUR")){
			if(miktar<=balance[1]){
				curRate = Double.valueOf(scurRate);
				sellRevenue = miktar*curRate;
				boolean isUpdated = balCont.updateBalanceSell(uid, "tl", sellCurr, String.valueOf(sellRevenue), String.valueOf(miktar), balance[0], balance[1], curRate);
				if(isUpdated){
					System.out.println(":::CONTROLLER ACCEPTED THE BALANCE UPDATE");
				}
			}
		}
		else if(base.equals("USD")){
			if(miktar<=balance[2]){
				curRate = Double.valueOf(scurRate);
				sellRevenue = miktar*curRate;
				boolean isUpdated = balCont.updateBalanceSell(uid, "tl", sellCurr, String.valueOf(sellRevenue), String.valueOf(miktar), balance[0], balance[2], curRate);
				if(isUpdated){
					System.out.println(":::CONTROLLER ACCEPTED THE BALANCE UPDATE");
				}
			}
		}
		
		
		double[] result = {curRate, sellRevenue};
		return result;
		
	}

	public double[] buyCurr(String uid, String buyCurr, String amount) throws IOException{
		double curRate = 0;
		double cost = 0; 
		double[] balance={0,0,0};
		String base="";
		String searchFor="TRY";
		double miktar = Double.valueOf(amount);
		
		
		if(buyCurr.equals("euro"))
			base = "EUR";
		else if(buyCurr.equals("dolar"))
			base = "USD";
		
		JsonObject currencyResult= currCont.getCurrencyRates(base);
		String scurRate = currencyResult.get(searchFor).toString();
		System.out.println("::"+base+"::SATIŞ KURU::"+scurRate);
		balance = balCont.getBalance(Integer.valueOf(uid));
		
		if(base.equals("EUR")){
			curRate = Double.valueOf(scurRate);
			cost = miktar*curRate;
			if(cost<=balance[0]){
				boolean isUpdated = balCont.updateBalanceBuy(uid, buyCurr, "tl", String.valueOf(cost), String.valueOf(miktar), balance[1], balance[0], curRate);
				if(isUpdated){
					System.out.println(":::CONTROLLER ACCEPTED THE BALANCE UPDATE");
				}
			}
			else{
				curRate = 0;
				cost = 0;
			}
		}
		else if(base.equals("USD")){
			curRate = Double.valueOf(scurRate);
			cost = miktar*curRate;
			if(cost<=balance[0]){
				boolean isUpdated = balCont.updateBalanceBuy(uid, buyCurr, "tl", String.valueOf(cost), String.valueOf(miktar), balance[2], balance[0], curRate);
				if(isUpdated){
					System.out.println(":::CONTROLLER ACCEPTED THE BALANCE UPDATE");
				}
			}
			else{
				curRate = 0;
				cost = 0;
			}
		}
		
		
		double[] result = {curRate, cost};
		return result;
		
	}
}
