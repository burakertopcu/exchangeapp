package controllers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.Statement;

public class LoginController {
	
	public int doLogin(String username, String pwd){
		
        Connection con = null;
        String message = null;
        int id = 0;
        try {
            
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            PreparedStatement statement = con.prepareStatement("SELECT id,username, pwd FROM users WHERE username = ? AND pwd = ?");
            statement.setString(1, username);
            statement.setString(2, pwd);
            ResultSet rs = statement.executeQuery();
            
            if(rs.next()){
                message = "LOGGED IN:)";
                System.out.println(":::sqldurum: "+message);
                id = rs.getInt(1);
                System.out.println(":::id:"+id);
            }else{
                message = "USERNAME OR PASSWORD IS INCORRECT!";
                System.out.println(":::sqldurum: "+message);
            }
        } catch (Exception e) {
            message = "FAILURE";
            e.printStackTrace();
        }
        return id;
    }
	
	public int doRegister(String username, String pwd){
		Connection con = null;
		String message;
		try {
            
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            
            String query = "insert into users (username,pwd) values(?,?)";
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, pwd);
            int rs = statement.executeUpdate();
            con.close();
            if(rs>0){
            	if(defineBalance(username, pwd)){
            		message = "SUCCESS";
                    System.out.println(":::sqldurum: "+message);
            	}
                
            }else{
                message = "CANNOT REGISTER!";
                System.out.println(":::sqldurum: "+message);
            }
            
        } catch (Exception e) {
            //message = "FAILURE";
            e.printStackTrace();
        }
		int id = doLogin(username,pwd);
		return id;
	}
	
	public boolean defineBalance(String username, String pwd){
		int id = doLogin(username, pwd);
		
		Connection con = null;
		String message;
		try {
            
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            
            String query = "insert into balances (uid,tl,euro,dolar) values(?,?,?,?)";
            PreparedStatement statement = con.prepareStatement(query);
            statement.setInt(1, id);
            statement.setString(2, "20000");
            statement.setString(3, "0");
            statement.setString(4, "0");
            int rs = statement.executeUpdate();
            con.close();
            if(rs>0){
                message = "SUCCESS";
                System.out.println(":::sqldurum: "+message);
                return true;
            }else{
                message = "CANNOT REGISTER!";
                System.out.println(":::sqldurum: "+message);
            }
            
        } catch (Exception e) {
            //message = "FAILURE";
            e.printStackTrace();
        }
		return false;
	}
	
	public boolean createDatabase() throws Exception {
	    String query = "CREATE DATABASE IF NOT EXISTS burakTest";
	    Statement st = null;
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
	    st = (Statement) con.createStatement();
	    if (st.executeUpdate(query) == 1) { // Then database created
	        System.out.println(":::Database created");
	        //return true;
	    }
	    st.close();
	    
	    Statement st2 = null;
	    Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
	    st2 = (Statement) con2.createStatement();
	    
	    String table1Query = "CREATE TABLE IF NOT EXISTS users ( id int(11) NOT NULL AUTO_INCREMENT, username varchar(45) COLLATE utf8_turkish_ci NOT NULL,pwd varchar(45) COLLATE utf8_turkish_ci NOT NULL,PRIMARY KEY (id),UNIQUE KEY username_UNIQUE (username)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;";
	    String table2Query = "CREATE TABLE IF NOT EXISTS balances (uid int(11) NOT NULL,tl float NOT NULL DEFAULT 20000,euro float NOT NULL,dolar float NOT NULL,UNIQUE KEY uid_UNIQUE (uid)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	    String table3Query = "CREATE TABLE IF NOT EXISTS transactions (uid int(11) NOT NULL,buyOrSell varchar(45) COLLATE utf8_turkish_ci NOT NULL,currency varchar(45) COLLATE utf8_turkish_ci NOT NULL,amount float NOT NULL,currencyRate float NOT NULL,date datetime NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;";
	    
	    if (st2.executeUpdate(table1Query) == 1) { // Then tables created
	        System.out.println(":::Table1 is created!!:::");
	        //return true;
	    }
	    if(st2.executeUpdate(table2Query) == 1){
	    	System.out.println(":::Table2 is created!!:::");
	    	//st2.close();
	    	//return true;
	    }
	    if(st2.executeUpdate(table3Query) == 1){
	    	System.out.println(":::Table3 is created!!:::");
	    	//st2.close();
	    	return true;
	    }
	    
	    return false;
	    
	    
	}
}
