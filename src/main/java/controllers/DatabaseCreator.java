package controllers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.Statement;

public class DatabaseCreator {

	public boolean createDatabase() throws Exception {
	    String query = "CREATE DATABASE IF NOT EXISTS burakTest1";
	    Statement st = null;
	    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
	    st = (Statement) con.createStatement();
	    if (st.executeUpdate(query) == 1) { // Then database created
	        System.out.println("Database created");
	        return true;
	    }
	    return false;
	}
	
}
