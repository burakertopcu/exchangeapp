package controllers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BalanceController {
	
	
	public double[] getBalance(int id){
		double[] balances = {0,0,0};
		Connection con = null;
        String message = null;
        double tl, euro, dolar;
        //System.out.println(":::balanceUserID:"+id);
        try {
            
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            PreparedStatement statement = con.prepareStatement("SELECT tl,euro,dolar FROM balances WHERE uid = ?");
            statement.setString(1, Integer.toString(id));
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                message = "::BALANCES RETRIEVED::)";
                //System.out.println(":::sqldurum: "+message);
                tl = rs.getFloat(1);
                euro = rs.getFloat(2);
                dolar = rs.getFloat(3);
                //System.out.println(":::tl:"+tl+" :::euro:"+euro+" :::dolar:"+dolar);
                balances[0] = tl;
                balances[1] = euro;
                balances[2] = dolar;
            }else{
                message = "hata oluştu!";
                System.out.println(":::sqldurum: "+message);
            }
        } catch (Exception e) {
            message = "FAILURE";
            e.printStackTrace();
        }
        return balances;
    }
	
	public boolean updateBalanceSell(String id, String pos, String neg, String revenue, String amountNeg, double existingPos, double existingNeg, double curRate){
		Connection con = null;
		String message = null;
		PreparedStatement statement = null;
		try {
            
			
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            
            if(neg.equals("euro")){
            	
            	statement = con.prepareStatement("update balances set euro = ?, tl = ?  WHERE uid = ?");
                double euroSum = existingNeg - Double.valueOf(amountNeg);
            	double tlSum = existingPos + Double.valueOf(revenue);
            	
            	System.out.print(":::SellCurrSQL::id:"+id+" euroSum:"+euroSum+" tlSum:"+tlSum);
            	
            	statement.setString(1, String.valueOf(euroSum));
            	statement.setString(2, String.valueOf(tlSum));
            	statement.setString(3, id);
                int rs = statement.executeUpdate();
                statement.close();
                // if balance is updated..
                if(rs > 0){
                    message = "::BALANCES UPDATED::)";
                    System.out.println(":::sqldurum: "+message+ "for user::"+id);
                    
                    statement = con.prepareStatement("insert into transactions (uid, buyOrSell, currency, amount, currencyRate, date ) values(?,?,?,?,?,?)");
                    
                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(System.currentTimeMillis());
                    String transDate = formatter.format(date);
                    
                    
                    System.out.println("TRANSACTION DATE::"+transDate);
                    statement.setString(1, id);
                	statement.setString(2, "sell");
                	statement.setString(3, neg);
                	statement.setString(4, amountNeg);
                	statement.setString(5, String.valueOf(curRate));
                	statement.setString(6, transDate);
                    int res = statement.executeUpdate();
                    statement.close();
                    
                    if(res>0){
                    	message = "::TRANSACTION RECORDED::)";
                        System.out.println(":::sqldurum: "+message+ "for user::"+id);
                        
                    }
                    
                    return true;
                }else{
                    message = "hata oluştu!";
                    System.out.println(":::sqldurum: "+message);
                    return false;
                }
                
            }
            else if(neg.equals("dolar")){
            	statement = con.prepareStatement("update balances set dolar = ?, tl= ?  WHERE uid = ?");
                
            	double dolarSum = existingNeg - Double.valueOf(amountNeg);
            	double tlSum = existingPos + Double.valueOf(revenue);
            	
            	statement.setString(1, String.valueOf(dolarSum));
            	statement.setString(2, String.valueOf(tlSum));
            	statement.setString(3, id);
                int rs = statement.executeUpdate();
                if(rs > 0){
                    message = "::BALANCES UPDATED::)";
                    System.out.println(":::sqldurum: "+message+ "for user::"+id);
                    
statement = con.prepareStatement("insert into transactions (uid, buyOrSell, currency, amount, currencyRate, date ) values(?,?,?,?,?,?)");
                    
                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(System.currentTimeMillis());
                    String transDate = formatter.format(date);
                    
                    
                    System.out.println("TRANSACTION DATE::"+transDate);
                    statement.setString(1, id);
                	statement.setString(2, "sell");
                	statement.setString(3, neg);
                	statement.setString(4, amountNeg);
                	statement.setString(5, String.valueOf(curRate));
                	statement.setString(6, transDate);
                    int res = statement.executeUpdate();
                    statement.close();
                    
                    if(res>0){
                    	message = "::TRANSACTION RECORDED::)";
                        System.out.println(":::sqldurum: "+message+ "for user::"+id);
                        
                    }
                    
                    return true;
                }else{
                    message = "hata oluştu!";
                    System.out.println(":::sqldurum: "+message);
                    return false;
                }
            }
            
            
        } catch (Exception e) {
            message = "FAILURE";
            e.printStackTrace();
        }
		
		return false;
	}
	
	public boolean updateBalanceBuy(String id, String pos, String neg, String cost, String amountPos, double existingPos, double existingNeg, double curRate){
		Connection con = null;
		String message = null;
		PreparedStatement statement = null;
		try {
            
			
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            
            if(pos.equals("euro")){
            	
            	statement = con.prepareStatement("update balances set euro = ?, tl = ?  WHERE uid = ?");
                double euroSum = existingPos + Double.valueOf(amountPos);
            	double tlSum = existingNeg - Double.valueOf(cost);
            	
            	System.out.print(":::SellCurrSQL::id:"+id+" euroSum:"+euroSum+" tlSum:"+tlSum);
            	
            	statement.setString(1, String.valueOf(euroSum));
            	statement.setString(2, String.valueOf(tlSum));
            	statement.setString(3, id);
                int rs = statement.executeUpdate();
                statement.close();
                // if balance is updated..
                if(rs > 0){
                    message = "::BALANCES UPDATED::)";
                    System.out.println(":::sqldurum: "+message+ "for user::"+id);
                    
                    statement = con.prepareStatement("insert into transactions (uid, buyOrSell, currency, amount, currencyRate, date ) values(?,?,?,?,?,?)");
                    
                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(System.currentTimeMillis());
                    String transDate = formatter.format(date);
                    
                    
                    System.out.println("TRANSACTION DATE::"+transDate);
                    statement.setString(1, id);
                	statement.setString(2, "buy");
                	statement.setString(3, pos);
                	statement.setString(4, amountPos);
                	statement.setString(5, String.valueOf(curRate));
                	statement.setString(6, transDate);
                    int res = statement.executeUpdate();
                    statement.close();
                    
                    if(res>0){
                    	message = "::TRANSACTION RECORDED::)";
                        System.out.println(":::sqldurum: "+message+ "for user::"+id);
                        
                    }
                    
                    return true;
                }else{
                    message = "hata oluştu!";
                    System.out.println(":::sqldurum: "+message);
                    return false;
                }
                
            }
            else if(pos.equals("dolar")){
            	statement = con.prepareStatement("update balances set dolar = ?, tl= ?  WHERE uid = ?");
                
            	double dolarSum = existingPos + Double.valueOf(amountPos);
            	double tlSum = existingNeg - Double.valueOf(cost);
            	
            	statement.setString(1, String.valueOf(dolarSum));
            	statement.setString(2, String.valueOf(tlSum));
            	statement.setString(3, id);
                int rs = statement.executeUpdate();
                if(rs > 0){
                    message = "::BALANCES UPDATED::)";
                    System.out.println(":::sqldurum: "+message+ "for user::"+id);
                    
statement = con.prepareStatement("insert into transactions (uid, buyOrSell, currency, amount, currencyRate, date ) values(?,?,?,?,?,?)");
                    
                    SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(System.currentTimeMillis());
                    String transDate = formatter.format(date);
                    
                    
                    System.out.println("TRANSACTION DATE::"+transDate);
                    statement.setString(1, id);
                	statement.setString(2, "buy");
                	statement.setString(3, pos);
                	statement.setString(4, amountPos);
                	statement.setString(5, String.valueOf(curRate));
                	statement.setString(6, transDate);
                    int res = statement.executeUpdate();
                    statement.close();
                    
                    if(res>0){
                    	message = "::TRANSACTION RECORDED::)";
                        System.out.println(":::sqldurum: "+message+ "for user::"+id);
                        
                    }
                    
                    return true;
                }else{
                    message = "hata oluştu!";
                    System.out.println(":::sqldurum: "+message);
                    return false;
                }
            }
            
            
        } catch (Exception e) {
            message = "FAILURE";
            e.printStackTrace();
        }
		
		return false;
	}
	
	public String getTransactions(int id){
		String message ="";
		Connection con = null;
        String result = "<table class=\"table\"><tr><th>Geçmiş İşlemler:</th><th></th><th></th><th></th><th></th></tr>"
        		+ "<tr><th>Alım/Satım:</th><th>Döviz Cinsi:</th><th>Miktar:</th><th>Kur:</th><th>Tarih:</th></tr>";
		
		try {
            
        	Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/burakTest","root","");
            PreparedStatement statement = con.prepareStatement("SELECT buyOrSell,currency,amount,currencyRate,date FROM transactions WHERE uid = ? ORDER BY date DESC");
            statement.setString(1, Integer.toString(id));
            ResultSet rs = statement.executeQuery();
            //statement.close();
            while(rs.next()){
            	String buyOrSell = rs.getString(1);
            	String currency = rs.getString(2);
            	double amount = rs.getFloat(3);
            	double currencyRate = rs.getFloat(4);
            	String currencyRatio = String.format("%.2f",currencyRate);
            	java.sql.Timestamp dbSqlDate = rs.getTimestamp(5);
            	//java.util.Date tarih = new java.util.Date(dbSqlDate.getTime());
            	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
            	String strDate = dateFormat.format(dbSqlDate); 
            	//String strDate = "";
            	
            	
            	result+="<tr>"+"<td>"+buyOrSell+"</td>";
            	result+="<td>"+currency+"</td>";
            	result+="<td>"+amount+"</td>";
            	result+="<td>"+currencyRatio+"</td>";
            	result+="<td>"+strDate+"</td>";
            	
            	result+="</tr>";
            }
            result+="</table>";
            //System.out.println("::result::"+result);
        } catch (Exception e) {
            message = "FAILURE";
            e.printStackTrace();
        }
		
		
		return result;
		
	}
}
