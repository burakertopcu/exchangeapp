package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import controllers.ExchangeController;

/**
 * Servlet implementation class BuySellServlet
 */
@WebServlet("/buysell")
public class BuySellServlet extends HttpServlet {
	ExchangeController controller = new ExchangeController();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuySellServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("::exchange geldi...");
		double[] result = {0,0};
		Map<String, Object> map = new HashMap<String, Object>();
		
		String opcode = request.getParameter("opcode");
		String id = request.getParameter("id");
		String amount = request.getParameter("amount");
		String currtype = request.getParameter("currtype").toLowerCase();
		
		System.out.println(":::opcode"+opcode+"::id:"+id+"::amount:"+amount+"::currType:"+currtype);
		
		if(opcode.equals("2")){
			result = controller.sellCurr(id, currtype, amount);
		}
		else if(opcode.equals("1")){
			result = controller.buyCurr(id, currtype, amount);
		}
		
		map.put("result", result);
		
		write(response, map);
	}
	
	private void write(HttpServletResponse response, Map<String, Object> map) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}

}
