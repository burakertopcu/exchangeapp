package controllers;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CurrencyController {

	public boolean doExchange(String buyCurrency, float buyAmount, float currency){
		
		
		return false;
	}
	
	public JsonObject getCurrencyRates(String base) throws IOException{
		
		// Setting URL
		
		String url_str = "https://api.exchangerate-api.com/v4/latest/"+base;

		// Making Request
		URL url = new URL(url_str);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();

		// Convert to JSON
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonObject jsonobj = root.getAsJsonObject();

		// Accessing object
		//System.out.println(jsonobj.get("rates"));
		//String result = jsonobj.get("rates").toString();
		jsonobj = (JsonObject) jsonobj.get("rates");
		
		return jsonobj;
	}
	
	
}
