package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import controllers.LoginController;

/**
 * Servlet implementation class LoginServlet1
 */
@WebServlet("/login")
public class LoginServlet1 extends HttpServlet {
	LoginController controller = new LoginController();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("arkaya geldi!!");
		Map<String, Object> map = new HashMap<String, Object>();
		boolean isValid = false;
		boolean isDbCreated = false;
		
		try {
			isDbCreated = controller.createDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String username = request.getParameter("username");
		String pwd = request.getParameter("pwd");
		int id=0;
		System.out.println("::: username:"+username+" :::pwd:"+pwd);
		if(username != null && username.trim().length() != 0){
			isValid = true;
			map.put("username", username);
			map.put("pwd", pwd);
			id=controller.doLogin(username,pwd);
			System.out.println("::user id::"+id);
			if(id == 0){
				id = controller.doRegister(username,pwd);
			}
			//controller.doRegister(username,pwd);
			
		}
		map.put("isValid", isValid);
		map.put("id", id);
		write(response, map);
	}

	private void write(HttpServletResponse response, Map<String, Object> map) throws IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}

}
